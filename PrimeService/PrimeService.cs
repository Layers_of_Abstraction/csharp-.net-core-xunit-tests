﻿using System;

namespace Prime.Services
{
    public class PrimeService
    {
        public bool IsPrime(int candidate)
        {   
            if (candidate < 2)
            {
                return false;
            }
            //throw new NotImplementedException("Please create a test first");

            for (var divisor = 2; divisor <= Math.Sqrt(candidate); divisor++)
            {
                if (candidate % divisor == 0)
                {
                    return true;
                }
            }
            return true;
        }

        //Method is delibrately confusing as if 
        //developer made mistake. Because 2 is a
        //prime number.
        public bool IsNOTPrime(int candidate = 2)
        {
            if (candidate > 1)
            {
                return true;
            }
            throw new NotImplementedException("Please create a test first");
        }

        
    }
}