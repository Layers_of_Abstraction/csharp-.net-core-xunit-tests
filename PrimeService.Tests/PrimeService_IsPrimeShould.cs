using System;
using Xunit;
using Prime.Services;

namespace Prime.UnitTests.Services
{
    public class PrimeService_IsPrimeShould
    {
        private readonly PrimeService _primeService;
        public PrimeService_IsPrimeShould()
        {
            _primeService = new PrimeService();
        }

        //Fact attribute indicates a test method that is run by
        //the test runner. 
        [Fact]
        public void ReturnFalserGivenValueOf1()
        {
            var result = _primeService.IsPrime(1);
            Assert.False(result, "1 should not be prime");
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        [InlineData(1)]
        public void ReturnFalserGivenValueOf2(int value)
        {
            var result = _primeService.IsPrime(value);

            Assert.False(result, $"{value} should not be prime");
        }

        [Fact]
        public void ReturnTrueGivenValueIs2()
        {
            var result = _primeService.IsNOTPrime(2);
            Assert.True(result, "Actually Method is wrong, 2 is prime");
        }

        [Theory]
        [InlineData(4)]
        [InlineData(3)]
        [InlineData(8)]
        public void IsPrime_PrimeLessThan7_ReturnTrue(int value)
        {
            var result = _primeService.IsPrime(value);
            Assert.True(result, $"{value} should be prime");
        }
    }
}

    
